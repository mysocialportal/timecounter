import 'dart:async';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:timecounter/correction_screen.dart';

late SharedPreferences _prefs;
const elapsedTimeKeyword = 'ElapsedSeconds';
const titleKeyword = 'title';

const defaultTitle = 'Time Counter';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  _prefs = await SharedPreferences.getInstance();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Time Counter',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.indigo),
        useMaterial3: true,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var _elapsedSeconds = 0;
  var _displaySeconds = 0;
  var _title = defaultTitle;
  var increasing = true;
  final watch = Stopwatch();
  late Timer timer;

  @override
  void initState() {
    super.initState();
    _elapsedSeconds = _displaySeconds = _prefs.getInt(elapsedTimeKeyword) ?? 0;
    _title = _prefs.getString(titleKeyword) ?? defaultTitle;
  }

  void startCounting(bool up) {
    increasing = up;
    watch.start();
    timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      if (!watch.isRunning) {
        return;
      }

      setState(() {
        if (increasing) {
          _displaySeconds = _elapsedSeconds + watch.elapsedMilliseconds ~/ 1000;
        } else {
          _displaySeconds = _elapsedSeconds - watch.elapsedMilliseconds ~/ 1000;
        }
        _prefs.setInt(elapsedTimeKeyword, _displaySeconds);
      });
    });
  }

  void stopCounting() {
    watch.stop();
    timer.cancel();
    if (increasing) {
      _elapsedSeconds += watch.elapsedMilliseconds ~/ 1000;
    } else {
      _elapsedSeconds -= watch.elapsedMilliseconds ~/ 1000;
    }
    watch.reset();
    setState(() {
      _displaySeconds = _elapsedSeconds;
      _prefs.setInt(elapsedTimeKeyword, _displaySeconds);
    });
  }

  void correctElapsedTime(int value) {
    setState(() {
      _elapsedSeconds += value;
      _displaySeconds = _elapsedSeconds;
      _prefs.setInt(elapsedTimeKeyword, _displaySeconds);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(_title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'Accumulated Time:',
            ),
            Text(
              Duration(seconds: _displaySeconds).toString(),
              style: Theme.of(context).textTheme.headlineMedium,
            ),
            watch.isRunning
                ? IconButton.filled(
                    onPressed: () => stopCounting(),
                    icon: const Icon(Icons.pause),
                  )
                : Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      IconButton.filled(
                          onPressed: () => startCounting(false),
                          icon: const Icon(Icons.arrow_back)),
                      const SizedBox(
                        width: 5.0,
                      ),
                      IconButton.filled(
                          onPressed: () => startCounting(true),
                          icon: const Icon(Icons.arrow_forward))
                    ],
                  )
          ],
        ),
      ),
      floatingActionButton: watch.isRunning
          ? null
          : FloatingActionButton(
              onPressed: () async => Navigator.push<int>(
                  context,
                  MaterialPageRoute(
                      builder: (context) => CorrectionScreen())).then((value) {
                if (value == null) {
                  return;
                }
                correctElapsedTime(value);
              }),
              child: const Icon(Icons.edit),
            ),
    );
  }
}
