import 'package:flutter/material.dart';
import 'package:timecounter/util.dart';

class CorrectionScreen extends StatelessWidget {
  final hoursController = TextEditingController(text: '00');
  final minutesController = TextEditingController(text: '00');
  final secondsController = TextEditingController(text: '00');

  CorrectionScreen({super.key});

  Duration? buildDuration(BuildContext context, bool up) {
    final multiplier = up ? 1 : -1;
    final hours = int.tryParse(hoursController.text);
    if (hours == null) {
      buildSnackbar(context, "Hours isn't parsable to integer");
      return null;
    }
    final minutes = int.tryParse(minutesController.text);
    if (minutes == null) {
      buildSnackbar(context, "Minutes isn't parsable to integer");
      return null;
    }
    final seconds = int.tryParse(secondsController.text);
    if (seconds == null) {
      buildSnackbar(context, "Seconds isn't parsable to integer");
      return null;
    }
    return Duration(
      hours: multiplier * hours,
      minutes: multiplier * minutes,
      seconds: multiplier * seconds,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Time Correction'),
      ),
      body: Center(
        child: Column(
          children: [
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.5,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                    child: TextField(
                      controller: hoursController,
                      keyboardType: TextInputType.number,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      ':',
                      style: Theme.of(context).textTheme.headlineLarge,
                    ),
                  ),
                  Expanded(
                    child: TextField(
                      controller: minutesController,
                      keyboardType: TextInputType.number,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      ':',
                      style: Theme.of(context).textTheme.headlineLarge,
                    ),
                  ),
                  Expanded(
                    child: TextField(
                      controller: secondsController,
                      keyboardType: TextInputType.number,
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 10.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                IconButton.filled(
                  onPressed: () {
                    final delta = buildDuration(context, false);
                    if (delta == null) {
                      return;
                    }

                    final seconds = delta.inSeconds;
                    Navigator.pop(context, seconds);
                  },
                  icon: const Icon(Icons.remove),
                ),
                const SizedBox(
                  width: 5.0,
                ),
                IconButton.filled(
                    onPressed: () {
                      final delta = buildDuration(context, true);
                      if (delta == null) {
                        return;
                      }

                      final seconds = delta.inSeconds;
                      Navigator.pop(context, seconds);
                    },
                    icon: const Icon(Icons.add)),
              ],
            ),
            const SizedBox(
              height: 10.0,
            ),
            const Text('Will adjust the current time by this value.')
          ],
        ),
      ),
    );
  }
}
